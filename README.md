# Docker Spring Mysql
- docker-compose up --build

* * --build to build the spring app image

* If you haven't changed anything and just want to relaunch your docker no need for the --build option

```diff
- Make sure there are no other apps running on ports 8080 & 3306
