package com.projet.projetdevops.service.impl;

import java.util.List;
import org.springframework.stereotype.Service;
import com.projet.projetdevops.entity.User;
import com.projet.projetdevops.repository.UserRepository;
import com.projet.projetdevops.service.def.UserService;

@Service
public class UserServiceImpl implements UserService{
	
	private UserRepository userRepository;
	
	public UserServiceImpl(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	public List<User> getAllUsers() {
		return this.userRepository.findAll();
	}

	//Save User (add and update)
	@Override
	public void Save(User user) {
		userRepository.save(user);
	}

	@Override
	public void deleteUser(int id) {
		this.userRepository.deleteById(id);
	}

	@Override
	public User getUserById(int Id){
		return userRepository.findById(Id).get();
	}

}
