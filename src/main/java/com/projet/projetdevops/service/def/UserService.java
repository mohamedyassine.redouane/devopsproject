package com.projet.projetdevops.service.def;

import java.util.List;
import java.util.Optional;

import com.projet.projetdevops.entity.User;

public interface UserService {

	List<User> getAllUsers();
	User getUserById(int id);
	void Save(User user);
	void deleteUser(int id);
}
