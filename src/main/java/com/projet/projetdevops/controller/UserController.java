package com.projet.projetdevops.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import com.projet.projetdevops.entity.User;
import com.projet.projetdevops.service.def.UserService;

@Controller
@RequestMapping("/")
public class UserController {
	
	private UserService userService;
	
	public UserController(UserService userService) {
		this.userService = userService;
	}
	
	@GetMapping("/")
	public String indexFile(Model model) {
		//get list of the users from db
		List<User> users = userService.getAllUsers();
		//set list of the users as a model to the tab
		model.addAttribute("usersList", users);
		return "users-list";
	}
	
	@GetMapping("/list")
	public String getUsers(Model model) {
		//get list of the users from db
		List<User> users = userService.getAllUsers();
		//set list of the users as a model to the tab
		model.addAttribute("usersList", users);
		return "users-list";
	}
	
	@GetMapping("/showAddUserForm")
	public String showAddForm(Model model) {
		//Creat a User
		User user = new User();
		//set unser as a model to the form
		model.addAttribute("user", user);
		
		return "user-form";
	}
	@GetMapping("/showUpdateUserForm")
	public String showEditForm(@RequestParam("id")int id, Model model) {
		//get the user from db
		User user = userService.getUserById(id);
		//set unser as a model to the form
		model.addAttribute("user", user);

		return "user-form";
	}
	
	@PostMapping("/save")
	public String saveUser(@ModelAttribute("user") User theuser) {
		
		//save User
		userService.Save(theuser);
		//Redirection to list
		return "redirect:/list";
	}

	@GetMapping ("/delete")
	public String deleteUser(@RequestParam("id") int id ) {
		//delete the user
	userService.deleteUser(id);
		//redirect to index
		return "redirect:/list";
	}

}
